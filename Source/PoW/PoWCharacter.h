

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PoWCharacter.generated.h"

UCLASS(config=Game)
class APoWCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;
public:
	APoWCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	int Armour;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool CrouchTrue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool SprintTrue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animaton")
	bool IsFiring;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animaton")
	bool IsReloading;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animaton")
	bool IsAiming;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerDef")
	float Direction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerDef")
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
	int MagSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
	float ReloadTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
	float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
	FString GunName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
		FString GunType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
		FString ReloadType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
		int CreditCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GunInfo")
		int TotalAmmo;



protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }


};

