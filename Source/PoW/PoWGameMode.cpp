// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PoWGameMode.h"
#include "PoWCharacter.h"
#include "UObject/ConstructorHelpers.h"

APoWGameMode::APoWGameMode()
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn>PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/Characters"));
	//if (PlayerPawnBPClass.Class != NULL)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}

}

int APoWGameMode::G_UpdateMagSize(int gunCount)
{
	return MagSize[gunCount];
}

float APoWGameMode::G_UpdateDamage(int gunCount)
{
	return Damage[gunCount];
}

float APoWGameMode::G_UpdateReloadTime(int gunCount)
{
	return ReloadTime[gunCount];
}

float APoWGameMode::G_UpdateFireRate(int gunCount)
{
	return FireRate[gunCount];
}

FString APoWGameMode::G_UpdateGunName(int gunCount)
{
	return GunName[gunCount];
}

FString APoWGameMode::G_UpdateGunType(int gunCount)
{
	return GunType[gunCount];
}

FString APoWGameMode::G_UpdateReloadType(int gunCount)
{
	return ReloadType[gunCount];
}

int APoWGameMode::G_UpdateCreditCost(int gunCount)
{
	return CreditCost_Gun[gunCount];
}

FString APoWGameMode::H_UpdateHealName(int gunCount)
{
	return HealName[gunCount];
}

int APoWGameMode::H_UpdateCreditCost(int gunCount)
{
	return CostCredits_Heal[gunCount];
}

int APoWGameMode::H_UpdateRegenerationAmount(int gunCount)
{
	return RegenerationAmount[gunCount];
}

bool APoWGameMode::H_UpdateIsHealth(int gunCount)
{
	return IsHealth[gunCount];
}
