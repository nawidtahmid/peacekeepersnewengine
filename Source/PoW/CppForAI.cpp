// Fill out your copyright notice in the Description page of Project Settings.


#include "CppForAI.h"

// Sets default values for this component's properties
UCppForAI::UCppForAI()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCppForAI::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCppForAI::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

int UCppForAI::InventoryAccess(int ItemNum)
{
	int last;
	if (ItemNum >= 3)
	{
		ItemNum = ItemNum - 3;
	}
	last = GunInventory[ItemNum];
	return last;
}


int UCppForAI::G_UpdateMagSize(int gunCount)
{
	return MagSize[gunCount];
}

float UCppForAI::G_UpdateDamage(int gunCount)
{
	return Damage[gunCount];
}

float UCppForAI::G_UpdateReloadTime(int gunCount)
{
	return ReloadTime[gunCount];
}

float UCppForAI::G_UpdateFireRate(int gunCount)
{
	return FireRate[gunCount];
}

FString UCppForAI::G_UpdateGunName(int gunCount)
{
	return GunName[gunCount];
}

int UCppForAI::G_UpdateCreditCost(int gunCount)
{
	return CreditCost_Gun[gunCount];
}

void UCppForAI::G_BuyGun(int gun, int loc)
{
	GunInventory[loc] = gun;
}
